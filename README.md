# Plantilla TFM MUSTIC UEM

Plantilla para TFM del Máster Universitario en Seguridad TIC de la Universidad Europea de Madrid desarrollada por

	Javier Junquera Sánchez <javier@junquera.xyz>

Para configurarla editar el archivo `config.tex` e introducir datos del trabajo.

Si se desea utilizar la fuente privativa `calibri` poner a `1` la variable `useWinFont` e introducir los archivos de fuente en la carpeta fonts

En el archivo [Plantilla_TFM_MUSTIC_UEM.pdf](Plantilla_TFM_MUSTIC_UEM.pdf) puede verse un ejemplo.
